#include <p16f887.h>

#define TRUE 1
#define FASLE 0

#define PINATRIS   
#define PINBTRIS  
#define PINAANSEL
#define PINBANSEL 
#define PINAInput LATCABits.RAx   //Location of pin a
#define PINBInput LATCABits.RAx   //Location of pin b

static void (*callback)(char); //Passes 1 as parameter when Clockwise 0 when CC
static char lastValue;

void Rotary_Setup(void (*)(char));
void Rotary_Check();   //Calls callback when change occured

