#include "Relais.h"


void SetupRelais(){
	
	//char i;
	//for(i = 0; i < RELAISCOUNT; i++){
	//	RELAISTRIS |= 1 << i;
	//}
    
    //1 means led off, 0 means led on
    
	RELAISTRIS = 0;
    RELAISLAT  = 15;
    
    
}


void TurnOnRelais(char number){
	
    char mask = 1 << number;
    mask = ~mask;
	RELAISLAT &= mask;
	
}


void TurnOffRelais(char number){
	
    RELAISLAT |= 1 << number;
    
	//Stel number 2
	//char mask = 1 << number; //00000100
	//mask = ~mask;			 //11111011
	
	//RELAISTRIS &= mask;
	
	//00001101 register
	//11111011 mask
	
	//00001001 output
	
}


char RelaisIsEnabled(char number){
	
	return !(RELAISLAT &= 1 << number);
	
}


void ToggleRelais(char number){
	
	if(!RelaisIsEnabled(number)){
		TurnOnRelais(number);
	}else{
		TurnOffRelais(number);
	}
	
}