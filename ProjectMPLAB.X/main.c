/*
 * File:   main.c
 *
 * Created on November 16, 2017, 2:21 PM
 */

//Setup configuration
#define _XTAL_FREQ 500000

// CONFIG1
#pragma config FOSC = INTRC_NOCLKOUT// Oscillator Selection bits (INTOSCIO oscillator: I/O function on RA6/OSC2/CLKOUT pin, I/O function on RA7/OSC1/CLKIN)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled and can be enabled by SWDTEN bit of the WDTCON register)
#pragma config PWRTE = ON       // Power-up Timer Enable bit (PWRT enabled)
#pragma config MCLRE = OFF      // RE3/MCLR pin function select bit (RE3/MCLR pin function is digital input, MCLR internally tied to VDD)
#pragma config CP = OFF         // Code Protection bit (Program memory code protection is disabled)
#pragma config CPD = OFF        // Data Code Protection bit (Data memory code protection is disabled)
#pragma config BOREN = ON       // Brown Out Reset Selection bits (BOR enabled)
#pragma config IESO = OFF       // Internal External Switchover bit (Internal/External Switchover mode is disabled)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enabled bit (Fail-Safe Clock Monitor is disabled)
#pragma config LVP = ON         // Low Voltage Programming Enable bit (RB3/PGM pin has PGM function, low voltage programming enabled)

// CONFIG2
#pragma config BOR4V = BOR40V   // Brown-out Reset Selection bit (Brown-out Reset set to 4.0V)
#pragma config WRT = OFF        // Flash Program Memory Self Write Enable bits (Write protection off)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
#include <pic16f887.h>
#include "RotaryEncoder.h"
#include "Relais.h"
#include "Adc.h"

void CallbackRotary(char);
void Loop();

#define Test6

#ifdef Test1
char current = 0;
void main(void) {
    
    OSCCON = 0b00110000; //Set clock for 500khz
    
    //Rotary_Setup(CallbackRotary);
	SetupRelais();
    
    while(1){
        Loop();
    }
    
    return;
}

//Called as fast as possible on repeat by main
void Loop(){
	
	current++;
	if(current == 4) current = 0;
	TurnOnRelais(current);
	__delay_ms(500);
	TurnOffRelais(current);
    
}

//Automatically called when rotary has a change (by rotary check))
void CallbackRotary(char c){
    
}
#endif

#ifdef Test2
void main(void) {
  
    Rotary_Setup(CallbackRotary);
	SetupRelais();
	
	Loop();
	
    return;
}

//Called as fast as possible on repeat by main
void Loop(){
	ToggleRelais(0);
}

//Automatically called when rotary has a change (by rotary check))
void CallbackRotary(char c){
    
}
#endif

#ifdef Test3
void main(void) {

    Rotary_Setup(CallbackRotary);
	SetupRelais();
	
    while(1){
        Loop();
	}
    
    return;
}

//Called as fast as possible on repeat by main
void Loop(){
	Rotary_Check();
}

//Automatically called when rotary has a change (by rotary check))
void CallbackRotary(char c){
    ToggleRelais(0);
}
#endif

#ifdef Test4
static int selectedOutput = 0;

void main(void) {
  
    Rotary_Setup(CallbackRotary);
	SetupRelais();
	
	Loop();
	
    return;
}

//Called as fast as possible on repeat by main
void Loop(){
	Rotary_Check();
}

//Automatically called when rotary has a change (by rotary check))
void CallbackRotary(char c){
	TurnOffRelais(selectedOutput);
	
	if(c){
		//Clockwise rotation
		selectedOutput++;
		
		if(selectedOutput == RELAISCOUNT) selectedOutput = 0;
	}else{
		//Counter clockwise rotation
		selectedOutput--;
		
		if(selectedOutput > RELAISCOUNT) selectedOutput = RELAISCOUNT - 1;
	}
	
	TurnOnRelais(selectedOutput);
}
#endif

#ifdef Test5
void main(void) {

    Rotary_Setup(CallbackRotary);
	SetupRelais();
    InitADC();
    
    while(1){
        Loop();
	}
    
    return;
}

//Called as fast as possible on repeat by main
void Loop(){
    char value = GetADCValue();
    
    PORTA = 255;
    
    char i;
    for(i = 0; i < value; i++){
        TurnOnRelais(i);
    }
}

//Automatically called when rotary has a change (by rotary check))
void CallbackRotary(char c){
    
}
#endif

#ifdef Test6
void main(void) {

    Rotary_Setup(CallbackRotary);
	SetupRelais();
    InitADC();
    
    while(1){
        Loop();
	}
    
    return;
}

//Called as fast as possible on repeat by main
void Loop(){
    static char lastValue = 0;
    char value = GetADCValue();
    if(!CheckDiff(lastValue, value, Threshold)){
        return;
    }
    
    PORTA = 255;
    
    char i;
    for(i = 0; i < value; i++){
        TurnOnRelais(i);
    }
}

//Automatically called when rotary has a change (by rotary check))
void CallbackRotary(char c){
    
}
#endif