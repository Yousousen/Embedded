#ifndef ADC_H
#define	ADC_H

#include <pic16f887.h>
#include <xc.h>

#define TRUE 1
#define FALSE 0

#define _XTAL_FREQ 500000

#define POTTRIS     TRISEbits.TRISE2
#define POTANS      ANSELbits.ANS7
#define ADCSELECT   0b0111

#define MaxVolume    4
#define MinVolume    0
#define Threshold    3
const char StepSize = 255 / MaxVolume;

void InitADC();
void InitPot();
char GetADCValue();
char GetVolume(char input);

char CheckDiff(char first,char second,char threshhold);

#endif	/* ADC_H */