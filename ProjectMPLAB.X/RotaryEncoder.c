#include "RotaryEncoder.h"


void Rotary_Setup(void (*call)(char)){

    PINAANSEL = 0;
    PINBANSEL = 0;
    PINATRIS  = 0;
    PINBTRIS  = 0;

    callback = call;

}


void Rotary_Check(){

    //Set input in a variable so if it changes the code still works
    char A = PINAInput;
    char B = PINBInput;

    //Check is a change has occured AND a is falling
    if((!A) && (lastValue & 2)){

        //If B is now low
        if(B){
            //Clockwise rotation
            if(callback != 0) callback(TRUE);
        }else{
            //Counterclockwise rotation
            if(callback != 0) callback(FALSE);            
        }

    }

    lastValue = (A << 1) + B;

}
