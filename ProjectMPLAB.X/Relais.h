#ifndef RELAIS_H
#define	RELAIS_H

#include <pic16f887.h>
#include <xc.h>

#define TRUE 1
#define FALSE 0

#define RELAISTRIS  TRISA
#define RELAISLAT	PORTA //No LATA?

#define RELAISCOUNT 4

void SetupRelais();
void TurnOnRelais(char number);
void TurnOffRelais(char number);
char RelaisIsEnabled(char number);
void ToggleRelais(char number);


#endif	/* RELAIS_H */