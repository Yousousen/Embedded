#include "Adc.h"


void InitADC(){
    //ADCON0
    ADCON0bits.ADCS = 00;
    ADCON0bits.CHS  = ADCSELECT;
    ADCON0bits.ADON = 1;
    
    //ADCON1
    ADCON1bits.ADFM = 0;
    
    //Pot
    InitPot();
    
}

void InitPot(){
    POTTRIS = 1;
    POTANS  = 1;
}

char GetADCValue(){
    __delay_ms(1);
    ADCON0bits.GO = 1;
    
    while(ADCON0bits.GO)
        continue;
    
    return ADRESH;
}

char GetVolume(char input){
    char volume = input / StepSize;
    
    return volume;
}

char CheckDiff(char first,char second,char threshhold){
    char diff;
    if(first>second){
        diff = first - second;
    }else{
        diff = second - first;
    }
    return diff;
}